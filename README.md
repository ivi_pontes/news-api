# News search bar

News search bar is a server side rendering app that shows the articles based on the input inserted.

It was build using Javascript, Node.js, Express.js and Handlebars\
The app is responsive, accessible\
Not reliant on client-side frameworks (i.e. Angular, React) or libraries like jQuery\
Built using Javascript and node.js\
Progressively enhanced\
Deployed on Heroku - https://news-api-ivina.herokuapp.com/
Have a similar look and feel as ft.com\
Performs well over 3G networks

## Installation

Use the package manager NPM to install foobar.

```bash
npm i --save
```

Link for deployed app: https://news-api-ivina.herokuapp.com/

