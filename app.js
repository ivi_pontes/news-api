const express = require("express");
const app = express();
const fetch = require("node-fetch");
const exphbs = require('express-handlebars');

app.use(express.static('public'));

app.engine('hbs', exphbs({
    defaultLayout: 'main',
    extname: '.hbs'
}));

app.set('view engine', 'hbs');

app.get("/", async function (req, res) {
    
    let query = req.query.search;
    let result;
    if (query !== undefined) {        
         result = await fetch('https://api.ft.com/content/search/v1', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'X-Api-Key': '59cbaf20e3e06d3565778e7b505fc2fb2c3646f59001ce9b005bd375'
            },
            body: JSON.stringify({
                "queryString": query,
                "resultContext": {
                    "aspects": ["title", "lifecycle", "location", "summary", "editorial"],
                    "maxResults" : "20",
                    "offset": "0"
                }
            })
        }).then((res) =>  res.json())
            .then(x => x)
            .catch(e => console.log("error", e))
    }
    
    const articlesInformation = result && result.results[0].indexCount ? result.results[0].results.map(x => x) : [];
    res.render('home', {
        articlesInformation: articlesInformation
    });
});


app.listen(process.env.PORT || 3000, function () {
    console.log("Server is listening. Ready to accept requests!");
});
